import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { AboutWellcome } from './models/IAbout';
import { AboutMedio } from './models/IAbout';
import { AboutFinal } from './models/IAbout';



@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public aboutWellcome: AboutWellcome | any = {};
  initAbout = '';

  public aboutMedium: AboutMedio | any = {};
  medium = '';

  public aboutEnding: AboutFinal | any = {};
  ending = '';

  constructor() {
    //Empezando el inicio del about
    this.aboutWellcome.fraseBienvenida = '¿Quién ha construido esto?',
    this.aboutWellcome.fraseH1 = '¡Hola, bienvenid@ a esta sección!',
    this.aboutWellcome.fraseH3 = 'Voy a contarte un poquito sobre mi, porque un poquito de feedback siempre gusta',
    this.aboutWellcome.primerParrafo = 'Lo primero, ¡muchísimas gracias por visitar la web! Esta página es mi primer proyecto hecho con Angular, y ha llevado muchas horas de trabajo y dedicación. Pero, ¡ojo! Eso no quiere decir que ya esté todo hecho; hay que seguir mimando y mejorándola :) Solo así se consiguen siempre los mejores resultados. También querrás saber el por qué de dedicar esta web a la Mitología. Muy sencillo, ¡me encanta! Adoro curiosear todo tipo de información sobre los antiguos Dioses, de todas las culturas. Me parece algo muy interesante :D Uno de mis mitos griegos favoritos es el de Dafne y Apolo. También el de la pobre Medusa; ¿sabías que Pegaso nació de la sangre que brotó de su cabeza al ser cortada por Perseo?';

    //Continuando con el medio del about

    this.aboutMedium.fraseH1 = 'Sobre esta web',
    this.aboutMedium.parrafo1 = 'Como mencionaba al principio, esta web está construida con Angular. Consta de una Fake Api creada totalmente desde cero, que devuelvbe información sobre Dioses, Semidioses y humanos, así como sus imágenes.',
    this.aboutMedium.parrafo2 = 'En un futuro, las secciones se irán ampliando a más culturas con sus correspondientes Dioses, Semidioses y humanos, con posibilidad de crear tu propio personaje. Además, incluirá un apartado independiente con los mitos más famosos de las culturas conocidas.';

    //Ya terminando el final del about

    this.aboutEnding.fraseH1 = 'Sobre una servidora',
    this.aboutEnding.primerParrafo = 'Si has podido ver el dibujito tan cuqui de arriba, ya sabrás que mi nombre es Marta González y soy diseñadora gráfica. Tengo 27 años y, a día de hoy, llevo relativamente poco tiempo ejerciendo profesionalmente como diseñadora. Pero aunque lleve poco tiempo le pongo muchísimas ganas, empeño y formación, ya que me encanta mi trabajo y necesita de estar muy bien actualizado. Mi siguiente reto es empezar con el diseño de producto en 3D, aparte de sacarle buen provecho a la programación, por supuesto.',
    this.aboutEnding.segundoParrafo = 'Empezar a programar fue un golpe del destino, ya que empecé hace unos años pero, por circunstancias, no pude volver a tocarlo hasta este año; ahora estoy con mucha carrerilla, intentando aprender todo lo que pueda y sacarle el mayor provecho posible. Porque, como dicen por ahí: los diseñadores que saben programar, son como Unicornios... ¡Y a mí me encantan los Unicornios!';


   }

  ngOnInit(): void {
  }

}
