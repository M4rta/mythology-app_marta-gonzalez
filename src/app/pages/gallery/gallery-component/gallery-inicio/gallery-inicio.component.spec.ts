import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryInicioComponent } from './gallery-inicio.component';

describe('GalleryInicioComponent', () => {
  let component: GalleryInicioComponent;
  let fixture: ComponentFixture<GalleryInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
