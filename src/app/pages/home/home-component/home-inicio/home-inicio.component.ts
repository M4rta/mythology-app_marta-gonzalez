import { BandaWellcome } from './../models/Ihome';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-inicio',
  templateUrl: './home-inicio.component.html',
  styleUrls: ['./home-inicio.component.scss']
})
export class HomeInicioComponent implements OnInit {
  @Input() wellcome: BandaWellcome | any = {};

  constructor() {
   }

  ngOnInit(): void {
  }

}
