import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  phrase: string = 'Hecho con todo el cariño';
  name: string = 'Marta Gonzalez Rothemund';

  constructor() {
   }

  ngOnInit(): void {
  }

}
