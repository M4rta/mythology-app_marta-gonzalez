import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryMedioComponent } from './gallery-medio.component';

describe('GalleryMedioComponent', () => {
  let component: GalleryMedioComponent;
  let fixture: ComponentFixture<GalleryMedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryMedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryMedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
