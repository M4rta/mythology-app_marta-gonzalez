import { Component, OnInit, Input } from '@angular/core';
import { AboutWellcome } from './../models/IAbout'

@Component({
  selector: 'app-about-inicio',
  templateUrl: './about-inicio.component.html',
  styleUrls: ['./about-inicio.component.scss']
})
export class AboutInicioComponent implements OnInit {
  @Input() initAbout: AboutWellcome | any = {};
  constructor() { }

  ngOnInit(): void {
  }

}
