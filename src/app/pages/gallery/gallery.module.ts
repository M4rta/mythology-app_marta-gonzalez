import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery-component/gallery.component';
import { GalleryInicioComponent } from './gallery-component/gallery-inicio/gallery-inicio.component';
import { GalleryMedioComponent } from './gallery-component/gallery-medio/gallery-medio.component';
import { GalleryFinalComponent } from './gallery-component/gallery-final/gallery-final.component';


@NgModule({
  declarations: [GalleryComponent, GalleryInicioComponent, GalleryMedioComponent, GalleryFinalComponent],
  imports: [
    CommonModule,
    GalleryRoutingModule
  ]
})
export class GalleryModule { }
