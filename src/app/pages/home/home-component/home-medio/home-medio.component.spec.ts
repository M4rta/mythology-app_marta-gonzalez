import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMedioComponent } from './home-medio.component';

describe('HomeMedioComponent', () => {
  let component: HomeMedioComponent;
  let fixture: ComponentFixture<HomeMedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
