export interface BandaWellcome {
    fraseBienvenida: string;
    fraseH1: string;
    fraseH3: string;
    primerParrafo: string;
// tslint:disable-next-line: eofline
}

export interface HomeMedio {
    imagenMapamundi: string;
    imagenAlt: string;
    subtituloImagen: string;
    parrafoBajoImagen: string;
}

export interface HomeFinal {

    imagenMapamundi2: string;
    imagenAlt2: string;
    fraseH1: string;
    fraseH3: string;
    primerParrafo: string;
    segundoParrafo: string;
}
