import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryFinalComponent } from './gallery-final.component';

describe('GalleryFinalComponent', () => {
  let component: GalleryFinalComponent;
  let fixture: ComponentFixture<GalleryFinalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryFinalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryFinalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
