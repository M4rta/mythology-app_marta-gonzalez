import { Component, OnInit, Input } from '@angular/core';
import { AboutMedio } from './../models/IAbout'

@Component({
  selector: 'app-about-medio',
  templateUrl: './about-medio.component.html',
  styleUrls: ['./about-medio.component.scss']
})
export class AboutMedioComponent implements OnInit {
  @Input() medium: AboutMedio | any = {};

  constructor() { }

  ngOnInit(): void {
  }

}
