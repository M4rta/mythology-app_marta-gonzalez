import { MythologyApiService } from '../../../services/Mythology-fakeapi/mythology-api.service';
import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { GreekGalleryWellcome } from './models/IGreekGallery';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  public galeriaInicio: GreekGalleryWellcome | any = {};
  galeriaGriega1 = '';

  public gods: any = [];

  constructor(private godsList: MythologyApiService) {

    // Empezando con el constructor del inicio de la galería griega
    this.galeriaInicio.frasegaleria = 'Galería de l@s Dios@s griegos',
    this.galeriaInicio.fraseH1 = '¡Visitando el Monte Olimpo!',
    this.galeriaInicio.primerParrafo = '¡Hola de nuevo! Qué gusto verte por la galería de, nada más y nada menos, todos los personajes importantes de la Mitología griega. Aquí encontrarás las fichas de l@s Dios@s, Semidios@s y mortales más conocidos que componen esta interesante cultura. ¡Ponte cómod@ y disfruta del desfile!';
  }

  ngOnInit(): void {

    this.godsList.getGods().subscribe(
      (result) => {
        this.gods = result;
        console.log(this.gods);
      }
    );
  }

}
