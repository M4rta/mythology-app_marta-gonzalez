import { Component, OnInit, Input } from '@angular/core';
import { HomeFinal } from './../models/Ihome';

@Component({
  selector: 'app-home-final',
  templateUrl: './home-final.component.html',
  styleUrls: ['./home-final.component.scss']
})
export class HomeFinalComponent implements OnInit {
  @Input() finalizando: HomeFinal | any = {};
  constructor() { }

  ngOnInit(): void {
  }

}
