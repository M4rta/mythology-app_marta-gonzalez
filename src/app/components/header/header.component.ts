import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public home: string = 'HOME';
  public gallery: string = 'GRECIA';
  public about: string = 'ABOUT THIS PAGE';


  constructor() {
   }

  ngOnInit(): void {
  }

}
