import { Component, OnInit, Input } from '@angular/core';
import { GreekGalleryWellcome } from './../models/IGreekGallery';

@Component({
  selector: 'app-gallery-inicio',
  templateUrl: './gallery-inicio.component.html',
  styleUrls: ['./gallery-inicio.component.scss']
})
export class GalleryInicioComponent implements OnInit {
  @Input() galeriaGriega1: GreekGalleryWellcome | any = {};

  constructor() { }

  ngOnInit(): void {
  }

}
