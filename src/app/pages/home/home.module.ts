import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { HomeInicioComponent } from './home-component/home-inicio/home-inicio.component';
import { HomeMedioComponent } from './home-component/home-medio/home-medio.component';
import { HomeFinalComponent } from './home-component/home-final/home-final.component';



@NgModule({
  declarations: [HomeComponent, HomeInicioComponent, HomeMedioComponent, HomeFinalComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
