import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMedioComponent } from './about-medio.component';

describe('AboutMedioComponent', () => {
  let component: AboutMedioComponent;
  let fixture: ComponentFixture<AboutMedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutMedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutMedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
