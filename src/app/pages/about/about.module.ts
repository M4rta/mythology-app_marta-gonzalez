import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './../about/about-component/about.component';
import { AboutInicioComponent } from './about-component/about-inicio/about-inicio.component';
import { AboutMedioComponent } from './about-component/about-medio/about-medio.component';
import { AboutFinalComponent } from './about-component/about-final/about-final.component';


@NgModule({
  declarations: [AboutComponent, AboutInicioComponent, AboutMedioComponent, AboutFinalComponent],
  imports: [
    CommonModule,
    AboutRoutingModule
  ]
})
export class AboutModule { }
