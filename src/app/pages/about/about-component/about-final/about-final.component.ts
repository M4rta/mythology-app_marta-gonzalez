import { Component, OnInit, Input } from '@angular/core';
import { AboutFinal } from './../models/IAbout'

@Component({
  selector: 'app-about-final',
  templateUrl: './about-final.component.html',
  styleUrls: ['./about-final.component.scss']
})
export class AboutFinalComponent implements OnInit {
  @Input() ending: AboutFinal | any = {};

  constructor() { }

  ngOnInit(): void {
  }

}
