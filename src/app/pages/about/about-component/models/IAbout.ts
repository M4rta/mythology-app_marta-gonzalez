export interface AboutWellcome {
    fraseBienvenida: string;
    fraseH1: string;
    fraseH3: string;
    primerParrafo: string;
// tslint:disable-next-line: eofline
}

export interface AboutMedio {
    fraseH1: string;
    parrafo1: string;
    parrafo2: string;
}

export interface AboutFinal {

    fraseH1: string;
    primerParrafo: string;
    segundoParrafo: string;
}