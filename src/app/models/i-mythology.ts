export interface GreekMythology {
    id: number;
    image: string;
    imageAlt: string;
    nameCharacter: string;
    category: 'Diosa' | 'Semidiosa' | 'Humana' | 'Dios' | 'Semidios' | 'Humano';
    godOf: string;
    powers: string;
    family: string;
    description: string;
}

export interface JapaneseMythology {
    id: number;
    image: string;
    imageAlt: string;
    nameCharacter: string;
    category: 'Dios' | 'Semidios' | 'Humano';
    godOf: string;
    powers: string;
    family: string;
    description: string;
}

export interface NordicMythology {
    id: number;
    image: string;
    imageAlt: string;
    nameCharacter: string;
    category: 'Dios' | 'Semidios' | 'Humano';
    godOf: string;
    powers: string;
    family: string;
    description: string;
}

export interface Myths {
    id: number;
    nameMyth: string;
    cultureMyth: string;
    image: string;
    imageAlt: string;
    description: string;
}