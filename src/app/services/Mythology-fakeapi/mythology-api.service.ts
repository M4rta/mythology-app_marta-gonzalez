import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { URL } from '../../key/urls';
import { GreekMythology } from '../../models/i-mythology';


@Injectable({
  providedIn: 'root'
})
export class MythologyApiService {

  private greekGods: string = URL.urlGreek;

  constructor(private http: HttpClient) {
    /*empty */
   }

   public getGods(): Observable<GreekMythology[]> {
    return this.http.get('http://localhost:3000/GreekMythology').pipe(map((response:GreekMythology[]) => {
      if (!response) {
        throw new Error('Value expected!');
      } else {
        return response;
      }
    }),
    catchError((err) => {
      throw new Error(err.message);
    })
    )}
  }
