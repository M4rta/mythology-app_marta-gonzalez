import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { BandaWellcome } from './models/Ihome';
import { HomeMedio } from './models/Ihome';
import { HomeFinal } from './models/Ihome';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit{
    // tslint:disable-next-line: align
  public bandaWellcome: BandaWellcome | any = {};
  public mitadHome: HomeMedio | any = {};
  enLaMitad = '';

  public acabandoHome: HomeFinal | any = {};
  finalizando = '';

  constructor() {
  this.bandaWellcome.fraseBienvenida = 'Wellcome to... MYTHOLOGY!!',
  this.bandaWellcome.fraseH1 = '¡Hola, bienvenid@!',
  this.bandaWellcome.fraseH3 = 'Gracias por acompañarme en este viaje por la misteriosa mitología del Planeta Tierra',
  this.bandaWellcome.primerParrafo = 'Si estás aquí es por que te encanta, te interesa o te llama la atención la Mitología. ¡Estás de suerte! Hemos conseguido acceder a los archivos secretos de todos los dioses y diosas, semidioses y semidiosas o personajes importantes que componen esos mitos (que probablemente no tienen mucho de mitos...Hehehe). Según vayas sumergiéndote más en la misteriosa Mitología podrás ir desbloqueando más archivos secretos de más dioses de otras culturas y así hacerte todo un expert@ en la materia. Así que, ¡lo dicho! ¡Hazte con todos!... ¡Pero de dioses, no pokemons hahahahaha!';

  // Pasando al constructor de la mitad de la home

  this.mitadHome.imagenMapamundi = '../../../assets/images/mapamundi_1.jpg',
  this.mitadHome.imagenAlt = 'Mapamundi antiguo del Planeta Tierra',
  this.mitadHome.subtituloImagen = 'La Mitología se remonta a tiempos antiquísimos... Prácticamente desde que el hombre es hombre.',
  this.mitadHome.parrafoBajoImagen = 'Desde las primeras civilizaciones, los Dioses han estado acompañando al hombre. Le enseñaron el poder del fuego, el cultivo, la pesca, la escritura, astronomía, matemáticas... Algunos dicen también que les enseñaron a filosofar y las grandes Artes Marciales. A cambio, el hombre les veneraba como a, ¡sorpresa!, como a Dios@s, jajajaja. Encomendaban sus almas a ellos cuando hacían viajes largos y peligrosos, les pedían consejo para los grandes acontecimientos, les daban ofrendas y sacrificios a cambio de buenos deseos y venturas por su parte. Algunos se involucraban tanto con la humanidad que creaban híbridos de Dios y humano (ejem, ejem, como alguno que yo me sé, que pudo ser tu tataratataratataratataratataratatarabuelo y que empieza por Ze y termina por us... Y lo sabes. Y Julio Iglesias también, por eso toma ejemplo, como también nuestro gran amigo Charlie Sheen).';

  // Pasando al constructor del final de la home

  this.acabandoHome.imagenMapamundi2 = '../../../assets/images/mapamundi_2.jpg',
  this.acabandoHome.imagenAlt2 = 'Mapamundi antiguo del Planeta Tierra',
  this.acabandoHome.fraseH1 = '¿Qué te encontrarás en esta web?',
  this.acabandoHome.fraseH3 = 'Veremos la Mitología en las distintas civilizaciones humanas y cuáles son sus Dios@s, Semidios@s y mortales más famos@s',
  this.acabandoHome.primerParrafo = 'Como comentamos en la primera línea, aquí lo que te encontrarás son los archivos secretos de los Dios@s, Semidios@s y human@s más famosos de las distintas mitologías. Te contaremos todos los trapos sucios y las hazañas. También los amoríos y las fidelidades de esas estrellas tan altas y lejanas. Además, podrás ver con detalle sus descripciones y quiénes eran (como si fuera un perfil de Meetic cualquiera).',
  this.acabandoHome.segundoParrafo = 'Así que, lo dicho, si te gusta el salseo y la cultura, este es tu sitio idóneo. ¡Eso sí! Aquí: ver, oír y callar... La mayoría de Dios@s tienen el ego muy sensible. No querrás que te caiga una maldición y acabes como nuestra buena amiga Medusa, ¿verdad? ¡Pues lo dicho! Antes de pasar a la siguiente página, limpia tus zapatos y acicálate un poco, ¡los dioses te observan!';

}

  ngOnInit(): void {}

}
