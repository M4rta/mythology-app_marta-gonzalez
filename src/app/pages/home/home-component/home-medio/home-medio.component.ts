import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { HomeMedio } from './../models/Ihome';


@Component({
  selector: 'app-home-medio',
  templateUrl: './home-medio.component.html',
  styleUrls: ['./home-medio.component.scss']
})
export class HomeMedioComponent implements OnInit {
  @Input() enLaMitad: HomeMedio | any = {};
  constructor() { }

  ngOnInit(): void {
  }

}
