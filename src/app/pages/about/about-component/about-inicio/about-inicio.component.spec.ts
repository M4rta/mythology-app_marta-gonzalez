import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutInicioComponent } from './about-inicio.component';

describe('AboutInicioComponent', () => {
  let component: AboutInicioComponent;
  let fixture: ComponentFixture<AboutInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
